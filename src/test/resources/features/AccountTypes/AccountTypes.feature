Feature: Testing endpoints for Account Types

  @Positive
  Scenario Outline: This is to get account types by country
    When User gives the desired country as input <Country> and sends the GET request
    Then User should get the account types for the provided country

    Examples: 
      | Country      |
      | UnitedStates |
      | Canada       |

  @Negative
  Scenario: This is test to check status code 400 when country for account type is not provided
    When User sends nothing as country, then status code 400 is received

  @Negative
  Scenario: This is test to check status code 404 when query param for account type is not provided
    When User sends no query param, then status code 404 is received

  @Positive
  Scenario: This is to get account type by ID
    When User sends valid account type id with GET request, then the account type should be fetched

  @Negative
  Scenario: This is to check status code 400 for get account type invalid request
    When User sends invalid request, then status code 400 is received

  @Negative
  Scenario: This is test to check status code 404 for account type id not found
    When User sends non-existing account type id, then status code 404 is received
