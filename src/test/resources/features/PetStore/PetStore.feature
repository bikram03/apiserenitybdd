Feature: Testing endpoints for Pet Store

  @Positive
  Scenario Outline: This is to create pet in pet store
    When User gives the desired inputs <categoryName>, <name>, <photoURLs>, <tagName>, <status> and sends the POST request
    Then Pet should get created

    Examples: 
      | categoryName | name  | photoURLs          | tagName    | status    |
      | Dogs         | Husky | www.muellerllc.org | Tag String | available |

  @Negative
  Scenario: This is test to check status code 400 for incorrect body for creating pet
    When User sends incorrect body with POST request, then status code 400 is received

  @Negative
  Scenario Outline: This is test to check status code 405 for incorrect method for creating pet
    When User gives the desired inputs <categoryName>, <name>, <photoURLs>, <tagName>, <status> and sends the request to create a pet, then status code 405 is received

    Examples: 
      | categoryName | name  | photoURLs          | tagName    | status    |
      | Dogs         | Husky | www.muellerllc.org | Tag String | available |

  @Positive
  Scenario: This is to delete the created pet
    When User sends the created pet id with DELETE request, then the pet should get deleted

  @Negative
  Scenario: This is test to check status code 400 for trying to delete with incorrect request format
    When User sends incorrect request format with DELETE request, then status code 400 is received

  @Negative
  Scenario: This is test to check status code 404 for trying to delete already deleted pet
    When User sends the deleted pet id with DELETE request, then status code 404 is received
