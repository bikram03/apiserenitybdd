package com.apiserenitybdd.utils;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Random;

import org.json.JSONObject;

public class Utilities {

	/**
	 * This method preserves the order of the JSON format
	 * 
	 * @param object
	 * @return the ordered JSON object
	 */
	public static JSONObject orderJSONObject(JSONObject object) {
		try {
			Field changeMap = object.getClass().getDeclaredField("map");
			changeMap.setAccessible(true);
			changeMap.set(object, new LinkedHashMap<>());
			changeMap.setAccessible(false);
		} catch (IllegalAccessException | NoSuchFieldException e) {
			e.printStackTrace();
		}

		return object;
	}

	/**
	 * This method creates a unique int with current time stamp
	 * 
	 * @return unique String as int
	 */
	public static int getUniqueNumberFromTimeStamp() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd HH:mm:ss");
		String string = dateFormat.format(new Date());
		String result = string.replaceAll("[-.: ]", "");
		return Integer.parseInt(result);
	}
	
	/**
	 * This generates a random int of desired digits
	 * 
	 * @param n
	 * @return a random int of desired digits
	 */
	public static int generateRandomDigits(int n) {
		int m = (int) Math.pow(10, n - 1);
		return m + new Random().nextInt(9 * m);
	}

}
