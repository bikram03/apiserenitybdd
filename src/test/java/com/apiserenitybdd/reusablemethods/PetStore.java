package com.apiserenitybdd.reusablemethods;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import com.apiserenitybdd.utils.Utilities;

import io.restassured.response.ValidatableResponse;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class PetStore {

	static JSONObject root = null;

	@Step("This method creates Pet petID: {0}, categoryName: {1}, name: {2}, photoURLs: {3}, tagName: {4}, status: {5}")
	public ValidatableResponse createPet(int petID, String categoryName, String name, String[] photoURLs,
			String tagName, String status) {

		root = preparePostPustRequestBody(petID, categoryName, name, photoURLs, tagName, status);

		return SerenityRest.rest()
				.given()
					.contentType("application/json")
					.body(root.toString())
				.when()
					.post("/pet")
				.then();
	}

	@Step("This method validates the reponse from positve create pet POST action response: {0}")
	public void validateResponseBody(String response) {

		System.out.println("Actual Response: " + response);
		System.out.println("Expected Response: " + root.toString());
		assert (response.equals(root.toString()));
	}
	
	@Step("This method deletes the pet corresponding to provided pet id petID: {0}")
	public ValidatableResponse deleteCreatedPet(int petID) {
		
		return SerenityRest.rest()
				.given()
				.when()
					.header("api_key", "special-key")
					.delete("/pet/"+petID)					
				.then();
	}

	/**
	 * This method prepares the JSON for POST, PUT Request Body
	 * 
	 * @param petID
	 * @param categoryName
	 * @param name
	 * @param photoURLs
	 * @param tagName
	 * @param status
	 * @return the JSON object
	 */
	@Step("This method prepares the JSON for POST, PUT Request Body")
	public static JSONObject preparePostPustRequestBody(int petID, String categoryName, String name, String[] photoURLs,
			String tagName, String status) {

		JSONObject root = new JSONObject();
		root = Utilities.orderJSONObject(root);
		root.put("id", petID);

		JSONObject categoryObj = new JSONObject();
		categoryObj = Utilities.orderJSONObject(categoryObj);
		categoryObj.put("id", petID + 1);
		categoryObj.put("name", categoryName);
		root.put("category", categoryObj);

		root.put("name", name);
		root.put("photoUrls", photoURLs);

		JSONObject tagsObj = new JSONObject();
		tagsObj = Utilities.orderJSONObject(tagsObj);
		tagsObj.put("id", petID - 1);
		tagsObj.put("name", tagName);
		List<JSONObject> tags = new ArrayList<JSONObject>();
		tags.add(tagsObj);
		root.put("tags", tags);

		root.put("status", status);

		return root;

	}

}
