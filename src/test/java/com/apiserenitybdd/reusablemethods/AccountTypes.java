package com.apiserenitybdd.reusablemethods;

import io.restassured.response.ValidatableResponse;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class AccountTypes {

	@Step("This method gets the Account Types as per country: {0}")
	public ValidatableResponse getAccountTypesByCountry(String country) {

		return SerenityRest.rest()
				.given()
					.queryParam("country", country)
				.when()
					.get("/AccountTypes")
				.then();

	}
	
	@Step("This method gets the Account Type as per id: {0}")
	public ValidatableResponse getAccountTypeByID(int id) {

		return SerenityRest.rest()
				.given()
				.when()
					.get("/AccountTypes/" + id)
				.then();

	}

}
