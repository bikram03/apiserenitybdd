package com.apiserenitybdd.cucumber;

import org.junit.runner.RunWith;

import com.apiserenitybdd.testbase.AccountTypesTestBase;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/AccountTypes")
public class AccountTypesRunnerTest extends AccountTypesTestBase{

}
