package com.apiserenitybdd.cucumber.steps;

import com.apiserenitybdd.reusablemethods.PetStore;
import com.apiserenitybdd.utils.Utilities;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.ValidatableResponse;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;

public class PetStoreSteps {

	@Steps
	PetStore petStore;

	static ValidatableResponse response;
	static int petID = Utilities.getUniqueNumberFromTimeStamp();

	// TC1
	@When("^User gives the desired inputs (.*), (.*), (.*), (.*), (.*) and sends the POST request$")
	public void postNewPet(String categoryName, String name, String photoURLs, String tagName, String status) {

		String[] photo_urls = { photoURLs };
		response = petStore.createPet(petID, categoryName, categoryName, photo_urls, tagName, status);
		

	}

	@Then("^Pet should get created$")
	public void verifyCreatePetResponse() {
		
		response.statusCode(200);
		petStore.validateResponseBody(response.extract().asString());
	}
	
	//TC2
	@When("^User sends incorrect body with POST request, then status code 400 is received$")
	public void createPetWithIncorrectBody() {
		
		ValidatableResponse res = SerenityRest.rest()
				.given()
					.contentType("application/json")
					.body("Incorrect Message Body")
				.when()
					.post("/pet")
				.then();
		
		res.statusCode(400);
		assert(res.extract().asString().contains("bad input"));
	}
	
	//TC3
	@When("^User gives the desired inputs (.*), (.*), (.*), (.*), (.*) and sends the request to create a pet, then status code 405 is received$")
	public void createNewPetWithIncorrectMethod(String categoryName, String name, String photoURLs, String tagName, String status) {

		String[] photo_urls = { photoURLs };

		ValidatableResponse res = SerenityRest.rest()
				.given()
					.contentType("application/json")
					.body(petStore.preparePostPustRequestBody(Utilities.generateRandomDigits(9), categoryName, name, photo_urls, tagName, status).toString())
				.when()
					.get("/pet")
				.then();
		
		res.statusCode(405);
		System.out.println(res.extract().asString());
	}
	
	//TC4
	@When("^User sends the created pet id with DELETE request, then the pet should get deleted$")
	public void deleteCreatedPet() {
		
		response = petStore.deleteCreatedPet(petID);
		response.statusCode(200);
		assert(response.extract().asString().contains("unknown"));
	}
	
	//TC5
	@When("^User sends incorrect request format with DELETE request, then status code 400 is received$")
	public void deletePetInvalidPetID() {
		
		SerenityRest.rest()
		.given()
		.when()
			.header("api_key", "special-key")
			.delete("/pet"+"InvalidPetID")					
		.then()
		.statusCode(400);
		System.out.println("Response: " + response.extract().asString());
	}
	
	//TC6
	@When("^User sends the deleted pet id with DELETE request, then status code 404 is received$")
	public void deleteDeletedPet() {
		
		response = petStore.deleteCreatedPet(petID);
		response.statusCode(404);
		System.out.println("Response: " + response.extract().asString());
	}

}
