package com.apiserenitybdd.cucumber.steps;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.apiserenitybdd.reusablemethods.AccountTypes;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class AccountTypesSteps {
	
	@Steps
	AccountTypes accountTypes;
	
	static ValidatableResponse response;
	static List<Object> IDList = new ArrayList<Object>();
	static List<Integer> IntegerIDList = new ArrayList<Integer>(); 
	static File schema = new File(System.getProperty("user.dir")+"/src/test/resources/JSONSchema/GetAccountTypesByCountrySchema.JSON");

	//TC1
	@Given("^User gives the desired country as input (.*) and sends the GET request$")
	public void getAccountTypesByCountry(String Country) {
		
		response = accountTypes.getAccountTypesByCountry(Country);
		response.statusCode(200);
		
		//This is for fetching and saving all account type ids
		IDList.addAll(response.extract().jsonPath().getList("accountTypes.id"));
		for(int i=0; i<IDList.size();i++) {
			IntegerIDList.add(Integer.parseInt(IDList.get(i).toString()));
		}
	}

	@Then("^User should get the account types for the provided country$")
	public void verifyAccountTypesResponse() {
		
		response.body(matchesJsonSchema(schema));
	}
	
	//TC2
	@When("^User sends nothing as country, then status code 400 is received$")
	public void verifyGetAccountTypeWithoutCountryResponse() {
		Response res = SerenityRest.rest()
						.given()
							.queryParam("country", "")
					 	.when()
					 		.get("/AccountTypes")
					 	.then()
					 		.statusCode(400)
					 		.extract().response();
		
		assert(res.jsonPath().get("message").equals("The request is invalid."));
	}
	
	//TC3
	@When("^User sends no query param, then status code 404 is received$")
	public void verifyGetAccountTypeWithoutQueryParamResponse() {
		Response res = SerenityRest.rest()
						.given()
					 	.when()
					 		.get("/AccountTypes")
					 	.then()
					 		.statusCode(404)
					 		.extract().response();
		
		assert(res.jsonPath().get("message").toString().startsWith("No HTTP resource was found that matches the request URI"));
	}
	
	//TC4
	@When("^User sends valid account type id with GET request, then the account type should be fetched$")
	public void getAccountTypeByID() {
		response = accountTypes.getAccountTypeByID(IntegerIDList.get(0));
		response.statusCode(200);
		
		response.body(matchesJsonSchema(schema));
		
		assert(response.extract().jsonPath().get("id").equals(IntegerIDList.get(0)));
	}
	
	//TC5
	@When("^User sends invalid request, then status code 400 is received$")
	public void verifyInvalidRequestResponse() {
		
		Response res = SerenityRest.rest()
						.given()
						.when()
							.get("/AccountTypes/InvalidRequest")
						.then()
							.statusCode(400)
							.extract().response();
		
		assert(res.jsonPath().get("message").equals("The request is invalid."));
	}
	
	//TC6
	@When("^User sends non-existing account type id, then status code 404 is received$")
	public void verifyNonExistingAccountTypeIDResponse() {
		Response res = SerenityRest.rest()
						.given()
					 	.when()
					 		.get("/AccountTypes/" + Collections.max(IntegerIDList)+1)
					 	.then()
					 		.statusCode(404)
					 		.extract().response();
		
		assert(res.jsonPath().get("message").equals("Account Type not found."));
	}	

}
